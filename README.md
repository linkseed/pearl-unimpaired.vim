# unimpaired.vim for Pearl

unimpaired.vim: Pairs of handy bracket mappings

## Details

- Plugin: https://github.com/tpope/vim-unimpaired
- Pearl: https://github.com/pearl-core/pearl
